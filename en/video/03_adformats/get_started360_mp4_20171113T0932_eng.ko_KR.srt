﻿1
00:00:00,645 --> 00:00:01,840
Facebook을 사용하면

2
00:00:01,841 --> 00:00:04,508
뉴스피드에서
완전히 새로운 방식으로

3
00:00:04,509 --> 00:00:06,980
중요한 주제나 장소에 생생하게 빠져들 수 있습니다

4
00:00:06,981 --> 00:00:10,477
어디에 있든
화면을 한 번만 터치하거나

5
00:00:11,746 --> 00:00:13,079
살짝 밀어넘기거나

6
00:00:14,619 --> 00:00:16,326
기기를 돌리는 것만으로 세상을 둘러볼 수 있습니다

7
00:00:21,700 --> 00:00:24,150
더 나아가 삼성 Gear VR에서 Facebook을 사용하면

8
00:00:24,151 --> 00:00:26,977
가장 손쉽고 간편하게
360도 동영상을 감상할 수 있습니다

9
00:00:26,978 --> 00:00:28,414
VR이 선사하는 현실감을 만끽하며

10
00:00:28,415 --> 00:00:31,572
이제까지 경험한 적 없는
새로운 세상을 만나보세요

11
00:00:36,766 --> 00:00:39,363
[캄차카 화산은 1999년 이래 국립 공원으로 보호받고 있으며
UNESCO 세계문화유산으로도 지정되었습니다]

12
00:00:48,888 --> 00:00:51,022
Facebook의 360도 동영상을 사용해보세요

13
00:00:53,186 --> 00:00:54,216
[facebook 360]

14
00:00:54,217 --> 00:00:56,015
[facebook360.fb.com]

